#!/bin/bash

IFACE=can0

# If interface is up, set it down first
# val=$(</sys/class/net/can0/operstate)
# echo $val
# if [[ val == "up" ]]; then
#     echo "Interface $IFACE is up, setting down first..."
    sudo ip link set $IFACE down
# fi

echo "Enabling interface $IFACE..."
sudo ip link set $IFACE up type can bitrate 1000000 dbitrate 1000000 fd on
sudo ifconfig $IFACE txqueuelen 65536
