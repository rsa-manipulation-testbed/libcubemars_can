#!/usr/bin/env python3


import serial
import argparse
import crc16
import time

from pathlib import Path


def read_packet(fp):

    header = fp.read(1)

    if header == b'\x02':
        packet_len = int.from_bytes(fp.read(1),"big")
    elif header == b'\x03':
        packet_len = int.from_bytes(fp.read(2),"big")
        return False
    else:
        print("Didn't find expected start of packet, got: %s" % header)
        return False

    #print("Packet length: %d" % packet_len)

    data = fp.read(packet_len)

    chksum = int.from_bytes(fp.read(2),"big")
    calc_chksum = crc16.crc16xmodem(data)

    if calc_chksum != chksum:
        print("Checksum mismatch %x != %x" % (chksum,calc_chksum))
    
    tailer = fp.read(1)
    if tailer != b'\x03':
        print("Did not get expected end of packet: got: %s" % tailer)
        return False

    return data.decode('ascii')


def send_packet(fp,data):

    if len(data) > 256:
        fp.write(b'\x03')
        fp.write(len(data).to_bytes(2,'big'))
    else:
        fp.write(b'\x02')
        fp.write(len(data).to_bytes(1,'big'))

    fp.write(data)

    print("Sending: %s" % data)
    chksum = crc16.crc16xmodem(data)
    fp.write(chksum.to_bytes(2,"big"))

    fp.write(b'\x03')

if __name__=="__main__":

    parser = argparse.ArgumentParser(description='Terminal which handles Cubemars serial protocol')
    parser.add_argument('port', type=Path,
                    help='Serial device or file to parse')

    args = parser.parse_args()

    if args.port.is_file():
        ## Just read a file
        with open(args.port,'rb') as fp:
            
            while True:
                data = read_packet(fp)
                if not data:
                    break

                data = data.strip()
                print("Read: %s" % data)

        with open("test.out", "wb") as fp:
            send_packet(fp,b'help\x0d\x0a')

    elif args.port.is_char_device():
        ## Serial port

        testout = open("test.out", "wb")

        with serial.Serial(str(args.port),baudrate=921500,timeout=1) as fp:

            #cmd = b'\x40exit\x0d\x0a'
            #cmd = b'\x1d'
            #cmd = b'\x6bhelp\x0d\x0a'

            send_packet(fp,b'\x1e')

            # cmd = b'\x01'
            # send_packet(fp,cmd)
            # # send_packet(testout,cmd)

            x = 0

            while True:
                data = read_packet(fp)
                if data:
                    data = data.strip()
                    print("Read: %s" % data)
                    if "exit" in data:
                        send_packet(fp,cmd)
                    continue
                else:
                    send_packet(fp,x.to_bytes(1,"big"))

                    x = x+1
                    if x > 108:
                        break



            
    else:
        print("Don't know how to open %s" % args.port)
