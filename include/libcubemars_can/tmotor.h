//
// Copyright 2021 University of Washington
//
// Released under the BSD license, see LICENSE in this repo

#pragma once

#include "libcubemars_can/tmotor_constants.h"
#include <linux/can.h>

namespace libcubemars_can {

namespace TMotor {

static const unsigned int CommandLength = 8;
static const unsigned int ReplyLength_V1 = 6;
static const unsigned int ReplyLength_V2 = 8;

// Struct for storing the current command _to_ the motor.
struct MotorCommand {
public:
  explicit MotorCommand(const TMotorConstants &c)
      : constants(c), _pos(0), _vel(0), _t_ff(0), _kp(0), _kd(0) {}

  // The char buffer _must_ be 8 bytes long
  void pack(unsigned char *) const;

  MotorCommand &setPos(float p);
  MotorCommand &setVel(float v);
  MotorCommand &setTorque(float tt);

  MotorCommand &setKp(float kp);
  MotorCommand &setKd(float kd);

  float pos() const { return _pos; }
  float vel() const { return _vel; }

  float Kd() const { return _kd; }
  float Kp() const { return _kp; }

  void dump() const;

  const TMotorConstants constants;

private:
  // These are private to allow bounds checking in setter
  float _pos, _vel, _t_ff;
  float _kp, _kd;
};

// Struct for storing state _from_ the motor
struct MotorState {
public:
  MotorState() {}
  MotorState(const unsigned char *data, const TMotorConstants &constants) {
    unpack(data, constants);
  }

  int id;
  float pos, vel, current;
  float temperature;
  int error;

  TMotorConstants::TMotorProtocol_t protocol;

  void unpack(const unsigned char *, const TMotorConstants &constants);
  void pack(int id, unsigned char *, const TMotorConstants &constants);

  void dump(void) const;
};

struct can_frame initFrame(unsigned int id);
struct can_frame enableCommand(unsigned int id);
struct can_frame disableCommand(unsigned int id);
struct can_frame zeroPositionCommand(unsigned int id);
struct can_frame motorCommand(unsigned int id, const MotorCommand &cmd);

void packEnableCommand(unsigned char *);
void packDisableCommand(unsigned char *);
void packZeroPositionCommand(unsigned char *);

MotorState unpackFrame(const struct can_frame &,
                       const TMotorConstants &constants);

} // namespace TMotor

} // namespace libcubemars_can
