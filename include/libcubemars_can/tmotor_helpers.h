//
// Copyright 2021 University of Washington
//
// Released under the BSD license, see LICENSE in this repo

#pragma once

namespace libcubemars_can {

    // Performs the float-to-int conversion as detailed in the TMotor manual.
    int float_to_uint(float x, float x_min, float x_max, unsigned int bits);

    // Performs the int-to-float conversaion as detailed in the TMotor manual.
    float uint_to_float(int x, float x_min, float x_max, int bits);

}  // namespace libcubemars_can
