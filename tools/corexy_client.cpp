// Copyright 2021 University of Washington
//
// Released under the BSD license, see LICENSE in this repo
//
// A simple command line client for running **two** TMotors.
// As designed, assumes the two motors are configured as a CoreXY
// transmission

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <stdint.h>
#include <cmath>
#include <csignal>
#include <sys/select.h>

#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>

#include <linux/can.h>
#include <linux/can/raw.h>

#include "libcubemars_can/tmotor.h"
#include "libcubemars_can/can_interface.h"
#include "libcubemars_can/tmotor_constants.h"

#include <iostream>

using namespace libcubemars_can;

bool isDone = false;
const float kp = 5.0;
const float kd = 2.5;

struct TMotorInstance {
    TMotorInstance(const std::string &n, const int can_id, const TMotorConstants constants)
        : initialized(false),
        name(n),
        id(can_id),
        cmd(constants)
    {
        cmd.setPos(0)
            .setVel(0)
            .setTorque(0)
            .setKp(0)
            .setKd(0);
    }

    void enable(CanInterface &canif) {
        struct can_frame txFrame = TMotor::enableCommand(id);
        std::cout << "Enabling " << name << std::endl;
        auto error = canif.send(txFrame);
        if (error < 0) {
            std::cerr << "Error sending enable to " << name << " : errno=" << error << std::endl;
            exit(-1);
        }
    }

    void disable(CanInterface &canif) {
        struct can_frame txFrame = TMotor::disableCommand(id);
        std::cout << "Disabling " << name << std::endl;
        auto error = canif.send(txFrame);
        if (error < 0) {
            std::cerr << "Error sending disable to " << name << " : errno=" << error << std::endl;
        }
    }


    void zeroPosition(CanInterface &canif) {
        struct can_frame txFrame = TMotor::zeroPositionCommand(id);
        std::cout << "Zeroing " << name << std::endl;
        auto error = canif.send(txFrame);
        if (error < 0) {
            std::cerr << "Error sending zero position request to " << name << " : errno=" << error << std::endl;
        }

        cmd.setPos(0);
    }

    void sendCmd(CanInterface &canif) {
        struct can_frame txFrame = TMotor::motorCommand(id, cmd);
        std::cout << "-- Sending to " << name << " --" << std::endl;
        cmd.dump();
        if (canif.send(txFrame) < 0) {
            std::cerr << "Error sending motor command (" << errno << "): "
                      << strerror(errno) << std::endl;
        }
    }

    void incrementPos( float f ) {
        cmd.setPos(cmd.pos() + f);
    }

    void process(const can_frame &frame) {
        auto state = TMotor::unpackFrame(frame, cmd.constants);

        std::cout << "-- Received from " << name << " --" << std::endl;
        state.dump();
        std::cout << "----------" << std::endl;

        if (!initialized) {
            std::cerr << "Initializing " << name << " with position: "
                        << state.pos << std::endl;

            cmd.setPos(state.pos)
                .setKd(kd)
                .setKp(kp);
            initialized = true;
        }
    }

    bool initialized;
    std::string name;
    const int id;
    TMotor::MotorCommand cmd;

};


TMotorInstance LeftMotor("Left", 1, TMotorConstants::AK80_6_V2_Constants),
               RightMotor("Right", 2, TMotorConstants::AK80_6_V2_Constants);

void signalHandler(int signum) {
    if (signum == SIGINT) {
        isDone = true;
        return;
    }

    exit(signum);
}


void nonblock(bool enable) {
    struct termios ttystate;

    // get the terminal state
    tcgetattr(STDIN_FILENO, &ttystate);

    if (enable) {
        // turn off canonical mode
        ttystate.c_lflag &= ~ICANON;
        // minimum of number input read.
        ttystate.c_cc[VMIN] = 1;
    } else {
        // turn on canonical mode
        ttystate.c_lflag |= ICANON;
    }

    // set the terminal attributes.
    tcsetattr(STDIN_FILENO, TCSANOW, &ttystate);
}

unsigned char kbhit() {
    struct timeval tv;
    fd_set fds;
    tv.tv_sec = 0;
    tv.tv_usec = 0;
    FD_ZERO(&fds);
    FD_SET(STDIN_FILENO, &fds);
    select(STDIN_FILENO+1, &fds, NULL, NULL, &tv);
    return FD_ISSET(STDIN_FILENO, &fds);
}

void read_reply(CanInterface &can) {
    struct can_frame frame;

    if (can.receive(frame)) {
        if (frame.can_dlc == 0) return;

        const unsigned char id = frame.data[0];

        if (id == LeftMotor.id) {
            LeftMotor.process(frame);
        } else if (id == RightMotor.id) {
            RightMotor.process(frame);
        } else {
            std::cerr << "Got bad id " << id << std::endl;
        }
    }
}

int main(int argc, char **argv) {
    unsigned char ch;
    nonblock(true);

    signal(SIGINT, signalHandler);

    if (argc < 2) {
        std::cerr << "Usage: " << argv[0] << " [CAN interface]" << std::endl;
        return -1;
    }

    auto canF = CanInterface::Create(argv[1]);
    if (!canF) {
        std::cerr << "Error creating CAN interface: "
                  << canF.error().msg() << std::endl;
        exit(-1);
    }

    auto canif(canF.value());

    std::cout << "Left using constants for: " << LeftMotor.cmd.constants.name << std::endl;
    std::cout << "Right using constants for: " << RightMotor.cmd.constants.name << std::endl;

    std::cout << "Press WASD to shift motor position." << std::endl;
    std::cout << "Press Q to quit" << std::endl;
    std::cout << "Pres [enter] to enable motor" << std::endl;
    std::string input;
    std::cin.ignore();

    /* send frame */
    LeftMotor.enable(canif);
    read_reply(canif);

    RightMotor.enable(canif);
    read_reply(canif);

    while (!isDone) {
        LeftMotor.sendCmd(canif);
        read_reply(canif);

        RightMotor.sendCmd(canif);
        read_reply(canif);

        const float delta = 0.5;

        if (kbhit() != 0) {
            ch = getchar();
            auto upch = toupper(ch);
            std::cout << "Got: " << ch << std::endl;
            if ((ch > 0) && (isprint(ch))) {
                if (upch == 'A') {
                    // Left
                    LeftMotor.incrementPos(delta);
                    RightMotor.incrementPos(delta);
                } else if (upch == 'D') {
                    // Right
                    LeftMotor.incrementPos(-delta);
                    RightMotor.incrementPos(-delta);
                } else if (upch == 'S') {
                    // Right
                    LeftMotor.incrementPos(delta);
                    RightMotor.incrementPos(-delta);
                } else if (upch == 'W') {
                    // Right
                    LeftMotor.incrementPos(-delta);
                    RightMotor.incrementPos(delta);
                } else if (upch == 'Z') {
                    LeftMotor.zeroPosition(canif);
                    read_reply(canif);

                    RightMotor.zeroPosition(canif);
                    read_reply(canif);
                } else if (upch == 'Q') {
                    isDone = true;
                }
            }
        }

        usleep(100000);
    }


    LeftMotor.disable(canif);
    read_reply(canif);

    RightMotor.disable(canif);
    read_reply(canif);

    nonblock(false);

    return 0;
}
