# libcubemars_can

Implements the CAN bus protocol for the [Cubemars AK-series](https://store.cubemars.com/category.php?id=122) of motors.  Also includes a sample application, a trivial simulator, and a generic SocketCAN interface.

# License

Released under the [BSD-3 License.](LICENSE)

